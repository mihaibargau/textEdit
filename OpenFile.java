import java.io.File;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class OpenFile {
	JFileChooser fileChooser = new JFileChooser();
	StringBuilder StringBuilder = new StringBuilder();
	public void pick() throws Exception{
		if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			if (!file.exists()) return;
			Scanner in = new Scanner(file);
			while(in.hasNext()) {
				StringBuilder.append(in.nextLine());
				StringBuilder.append("\n");
			}
			in.close();
			
		}
		else return;
		
	}
	

}
