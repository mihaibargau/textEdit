import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
//import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
//import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.UIManager;
//import javax.swing.JTextArea;
//import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.util.*;
public class TextEditorGUI extends JFrame {
	/**
	 * Launch the application.
	 */
	private static final long serialVersionUID = 0L;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					TextEditorGUI frame = new TextEditorGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TextEditorGUI() {
		setTitle("Text Edit\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 536, 457);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		JTextPane textPane = new JTextPane();
		textPane.setFont(new Font("Arial", Font.PLAIN, 13));
		scrollPane.setViewportView(textPane);
		
		JMenuBar menuBar = new JMenuBar();
		scrollPane.setColumnHeaderView(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			OpenFile openFile = new OpenFile();
			try {
			openFile.pick();
			}catch(Exception e) {
				e.printStackTrace();
			}
			textPane.setText(openFile.StringBuilder.toString());
			}
			
		});
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					JFileChooser choose = new JFileChooser();
					int r = choose.showSaveDialog(null);
					if (r == JFileChooser.CANCEL_OPTION) return;
					File select = choose.getSelectedFile();
					String content = textPane.getText();
					Formatter format = new Formatter(select);
					format.format("%s", content);
					format.close();
				}catch(Exception a) {
					a.printStackTrace();
				}
			}
		});
		mnFile.add(mntmSave);
		
		JMenuItem mntmClose = new JMenuItem("Close File");
		mntmClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText("");
			}
		});
		mnFile.add(mntmClose);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public  void actionPerformed(ActionEvent arg0) {
				try {
					System.exit(0);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		mnFile.add(mntmExit);
	}
}
	
	
